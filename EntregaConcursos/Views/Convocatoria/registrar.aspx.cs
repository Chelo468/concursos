﻿using EntregaConcursos.Models;
using EntregaConcursos.Models.Soporte;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EntregaConcursos.Views.Convocatoria
{
    public partial class registrar : System.Web.UI.Page
    {
        private Universidad universidad = new Universidad();
        private Concurso concurso = new Concurso();

        protected void Page_Load(object sender, EventArgs e)
        {
            
                registrarDatos();
            List<Carrera> carreras = obtenerCarreras();
            StringBuilder sb = new StringBuilder();
            if (!IsPostBack)
            {
                ddlCarreras.DataSource = carreras;
                ddlCarreras.DataTextField = "nombre";
                ddlCarreras.DataBind();
                cargosDiv.Visible = false;

            }

           
        }

        private List<Carrera> obtenerCarreras()
        {
            List<Carrera> carreras = new List<Carrera>();
            var iteradorUniv = universidad.crearIterador(universidad.Facultades.ToArray());

            while (!iteradorUniv.haTerminado())
            {
                var facultad = iteradorUniv.elementoActual() as Facultad;
                var iteradorFacultad = facultad.crearIterador(facultad.Carreras.ToArray());
                while (!iteradorFacultad.haTerminado())
                {
                    var carrera = iteradorFacultad.elementoActual() as Carrera;
                    carreras.Add(carrera);
                    iteradorFacultad.siguiente();
                }
                iteradorUniv.siguiente();
            }

            return carreras;
        }

        private void registrarDatos()
        {
            //Universidad universidad = new Universidad();
            universidad = new Universidad();
            List<Carrera> carreras = new List<Carrera>();
            List<Catedra> catedras = new List<Catedra>();
            Catedra catedra = new Catedra("123", "Catedra1", 1, "asd");
            catedras.Add(catedra);
            catedra = new Catedra("456", "Catedra2", 2, "qwe");
            catedras.Add(catedra);
            catedra = new Catedra("789", "Catedra3", 3, "zxc");
            catedras.Add(catedra);
            Carrera carrera = new Carrera("Carrera1", "descripcion", catedras);
            carreras.Add(carrera);
            catedras = new List<Catedra>();
            catedra = new Catedra("123", "Catedra4", 1, "asd");
            catedras.Add(catedra);
            catedra = new Catedra("456", "Catedra5", 2, "qwe");
            catedras.Add(catedra);
            catedra = new Catedra("789", "Catedra6", 3, "zxc");
            catedras.Add(catedra);
            carrera = new Carrera("Carrera2", "descripcion", catedras);
            carreras.Add(carrera);
            catedras = new List<Catedra>();
            catedra = new Catedra("123", "Catedra7", 1, "asd");
            catedras.Add(catedra);
            catedra = new Catedra("456", "Catedra8", 2, "qwe");
            catedras.Add(catedra);
            catedra = new Catedra("789", "Catedra9", 3, "zxc");
            catedras.Add(catedra);
            carrera = new Carrera("Carrera3", "descripcion", catedras);
            carreras.Add(carrera);

            universidad.Facultades.Add(new Facultad("Facultad1", carreras));

            carreras = new List<Carrera>();
            catedras = new List<Catedra>();

            catedra = new Catedra("123", "Catedra10", 1, "asd");
            catedras.Add(catedra);
            catedra = new Catedra("456", "Catedra11", 2, "qwe");
            catedras.Add(catedra);
            catedra = new Catedra("789", "Catedra12", 3, "zxc");
            catedras.Add(catedra);
            carrera = new Carrera("Carrera4", "descripcion", catedras);
            carreras.Add(carrera);
            catedras = new List<Catedra>();
            catedra = new Catedra("123", "Catedra13", 1, "asd");
            catedras.Add(catedra);
            catedra = new Catedra("456", "Catedra14", 2, "qwe");
            catedras.Add(catedra);
            catedra = new Catedra("789", "Catedra15", 3, "zxc");
            catedras.Add(catedra);
            carrera = new Carrera("Carrera5", "descripcion", catedras);
            carreras.Add(carrera);
            catedras = new List<Catedra>();
            catedra = new Catedra("123", "Catedra16", 1, "asd");
            catedras.Add(catedra);
            catedra = new Catedra("456", "Catedra17", 2, "qwe");
            catedras.Add(catedra);
            catedra = new Catedra("789", "Catedra18", 3, "zxc");
            catedras.Add(catedra);
            carrera = new Carrera("Carrera6", "descripcion", catedras);
            carreras.Add(carrera);
            catedras = new List<Catedra>();

            universidad.Facultades.Add(new Facultad("Facultad2", carreras));


            
            
        }

        protected void ddlCarreras_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<Catedra> catedrasDeCarrera = new List<Catedra>();
            var iteradorUniversidad = universidad.crearIterador(universidad.Facultades.ToArray());
            while (!iteradorUniversidad.haTerminado())
            {
                var facultad = iteradorUniversidad.elementoActual() as Facultad;
                var iteradorFacultad = facultad.crearIterador(facultad.Carreras.ToArray());
                while (!iteradorFacultad.haTerminado())
                {
                    if (iteradorFacultad.cumpleFiltros(new string[] { ddlCarreras.Text }))
                    {
                        var carrera = iteradorFacultad.elementoActual() as Carrera;
                        var iteradorCarrera = carrera.crearIterador(carrera.Catedras.ToArray());
                        iteradorCarrera.primero();
                        while (!iteradorCarrera.haTerminado())
                        {
                            var catedra = iteradorCarrera.elementoActual() as Catedra;
                            catedrasDeCarrera.Add(catedra);
                            iteradorCarrera.siguiente();
                        }
                    }
                    iteradorFacultad.siguiente();
                }
                iteradorUniversidad.siguiente();
            }

            grdCarreras.DataSource = catedrasDeCarrera;
            grdCarreras.DataBind();

        }

        protected void grdCarreras_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "seleccionarCatedra")
            {
                List<Categoria> categoriasDocente = new List<Categoria>();
                Categoria categoria = new Categoria("TITULAR", "CARGO TITULAR");
                categoriasDocente.Add(categoria);
                categoria = new Categoria("ADJUNTO", "CARGO ADJUNTO");
                categoriasDocente.Add(categoria);
                categoria = new Categoria("ASOCIADO", "CARGO ASOCIADO");
                categoriasDocente.Add(categoria);
                categoria = new Categoria("JTP", "CARGO JTP");
                categoriasDocente.Add(categoria);
                categoria = new Categoria("AYUDANTE 1RA", "CARGO AYUDANTE 1RA");
                categoriasDocente.Add(categoria);

                grdCargos.DataSource = categoriasDocente;
                grdCargos.DataBind();

                int index = Convert.ToInt32(e.CommandArgument);

                     
                GridViewRow row = grdCarreras.Rows[index];

                lblCatedra.InnerText ="Catedra: " + grdCarreras.Rows[index].Cells[2].Text;

                cargosDiv.Visible = true;
                
            }
        }

        protected void cargarCantidades(object sender, EventArgs e)
        {
            List<CargoLlamado> cargosLlamado = new List<CargoLlamado>();
            for (int i = 0; i < grdCargos.Rows.Count; i++)
            {
                TextBox txtCantidad = grdCargos.Rows[i].FindControl("txtCantidad") as TextBox;
                string cantidad = txtCantidad.Text;
                if(Convert.ToInt32(cantidad) > 0)
                {
                    Categoria categoria = new Categoria(grdCargos.Rows[i].Cells[0].Text, grdCargos.Rows[i].Cells[1].Text);
                    CargoLlamado cargoLlamado = new CargoLlamado(categoria,Convert.ToInt32(cantidad),new List<Inscripcion>());
                    cargosLlamado.Add(cargoLlamado);
                }
            }
            //concurso = new Concurso();
            concurso.Cargos = cargosLlamado;
            Session["concurso"] = concurso;
            paso1.Visible = false;
            paso2.Visible = true;
            
        }

        protected void btnConfirmarConcurso_Click(object sender, EventArgs e)
        {
            concurso = Session["concurso"] as Concurso;
            DateTime fechaEstInsc;
            DateTime fechaEstRealiza;
            if(DateTime.TryParse(txtFechaEstInsc.Text, out fechaEstInsc))
                concurso.FechaDesdeInscripcionAspirantes = fechaEstInsc;
            if (DateTime.TryParse(txtFechaEstInsc.Text, out fechaEstRealiza))
                concurso.FechaEstimadaRealizacion = fechaEstRealiza;
        }
    }
}