﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="registrar.aspx.cs" Inherits="EntregaConcursos.Views.Convocatoria.registrar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="paso1" runat="server" style="margin-top: 15px">

        <asp:Label ID="txtCarreras" runat="server" Text="Seleccione una carrera"></asp:Label>
        <asp:DropDownList ID="ddlCarreras" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCarreras_SelectedIndexChanged"></asp:DropDownList>
        <asp:GridView ID="grdCarreras" runat="server" AutoGenerateColumns="false" OnRowCommand="grdCarreras_RowCommand">
            <Columns>
                <asp:TemplateField>
                  <ItemTemplate>
                    <asp:Button ID="AddButton" runat="server" 
                      CommandName="seleccionarCatedra" 
                      CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                      Text="Seleccionar" />
                  </ItemTemplate> 
                </asp:TemplateField>
                <asp:BoundField DataField="Codigo" HeaderText="Codigo" />
                <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                <asp:BoundField DataField="Nivel" HeaderText="Nivel" />
                <asp:BoundField DataField="Sigla" HeaderText="Sigla" />
            </Columns>
        </asp:GridView>
        <div id="cargosDiv" runat="server">
            <label id="lblCatedra" runat="server"></label>
          <asp:GridView ID="grdCargos" runat="server" AutoGenerateColumns="False" >
            <Columns>
               <asp:BoundField DataField="Nombre" HeaderText="Cargo" />
               <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" />
               <asp:TemplateField HeaderText="Cantidad"  >
               <ItemTemplate>
               <asp:TextBox ID="txtCantidad" Enabled="true" Text="0" runat="server" ></asp:TextBox>

               </ItemTemplate>
               </asp:TemplateField>
            </Columns>
        </asp:GridView>
            <br />
            <br />
            <asp:Button Text="Enviar" ID="bntEnviar" runat="server" OnClick="cargarCantidades" />
        </div>
        
       
    </div>
    <div id="paso2" runat="server" visible="false" style="margin-top: 15px;">
        <asp:GridView ID="grdResumenConcurso" runat="server"></asp:GridView>
        <h2>Complete las fechas que se le solicitan a continuación</h2>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblFechaEstInsc" runat="server" Text="Fecha estimada de inscripcion para los aspirantes:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFechaEstInsc" runat="server" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFechaEstRealiza" runat="server" Text="Fecha estimada de realización del concurso:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFechaEstRealiza" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td rowspan="2"><asp:Button ID="btnConfirmarConcurso" runat="server" Text="Confirmar Convocatoria" OnClick="btnConfirmarConcurso_Click" /></td>
            </tr>
            
        </table>
        

    </div>
</asp:Content>
