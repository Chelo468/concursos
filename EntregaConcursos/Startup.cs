﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EntregaConcursos.Startup))]
namespace EntregaConcursos
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
