﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntregaConcursos.Models.Interfaces
{
    public interface IAgregado
    {
        IIterador crearIterador(object[] elementos);
    }
}
