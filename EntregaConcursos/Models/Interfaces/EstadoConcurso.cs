﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntregaConcursos.Models.Interfaces
{
    public abstract class EstadoConcurso
    {
        public virtual bool esAnulado()
        {
            return false;
        }
        public virtual bool esAprobado()
        {
            return false;
        }
        public  virtual bool esCerradoInscripcion()
        {
            return false;
        }
        public  virtual bool esConTribunalAsignado()
        {
            return false;
        }
        public  virtual bool esDesierto()
        {
            return false;
        }
        public  virtual bool esEnConsejoDirectivo()
        {
            return false;
        }
        public  virtual bool esEnEjecucion()
        {
            return false;
        }
        public  virtual bool esEnRevision()
        {
            return false;
        }
        public  virtual bool esFinalizado()
        {
            return false;
        }

        public  virtual bool esGenerado()
        {
            return false;
        }
        public  virtual bool esInscripcionAbierta()
        {
            return false;
        }
        public  virtual bool esNominaPublicada()
        {
            return false;
        }
        public  virtual bool esObservado()
        {
            return false;
        }
        public  virtual bool esPublicado()
        {
            return false;
        }
        public  virtual bool esRechazado()
        {
            return false;
        }

    }
}