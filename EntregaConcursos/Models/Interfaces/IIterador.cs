﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntregaConcursos.Models.Interfaces
{
    public interface IIterador
    {
        void crear(object[] listado);
        object elementoActual();
        bool haTerminado();
        object primero();
        void setLista(object[] listado);
        void setPosicion(int posicion);
        object siguiente();
        bool cumpleFiltros(string[] filtros);
    }
}
