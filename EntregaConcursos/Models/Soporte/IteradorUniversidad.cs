﻿using EntregaConcursos.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntregaConcursos.Models.Soporte
{
    public class IteradorUniversidad : IIterador
    {
        private Facultad[] facultades;
        private int actual;

        public IteradorUniversidad(object[] facultades)
        {
            this.facultades = facultades as Facultad[];
        }

        public void crear(Object[] facultades)
        {
            this.facultades = facultades as Facultad[];
            actual = -1;
        }

        public object elementoActual()
        {
            // Si no existen elementos, devolveremos null.
            // Si el indice actual es mayor que el mayor indice aceptable, devolveremos null.
            // Si el indice actual es -1, devolveremos null.
            if ((this.facultades == null) || (this.facultades.Length == 0) || 
                (actual > this.facultades.Length - 1) || (actual < 0))
                return null;

            // Devolvemos el elemento correspondiente al elemento actual
            else
                return facultades[actual];
        }

        public bool haTerminado()
        {
            return actual >= facultades.Length;
        }

        public object primero()
        {
            try
            {
                actual = 0;
                return facultades[0];
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void setLista(object[] facultades)
        {
            this.facultades = facultades as Facultad[];
        }

        public void setPosicion(int posicion)
        {
            actual = posicion;
        }

        public object siguiente()
        {
            // Si no existen elementos, devolveremos null.
            // Si el indice siguiente es mayor que el mayor indice aceptable, devolveremos null.
            if ((facultades == null) ||
                (facultades.Length == 0) ||
                (actual + 1 > facultades.Length - 1))
            {
                actual++;
                return null;
            }

            // Aumentamos el índice en una unidad y devolvemos ese elemento
            else
            {
                actual++; 
                return facultades[actual];
            }
        }

        public bool cumpleFiltros(string[] filtros)
        {
            if (filtros.Length > 1 || filtros.Length < 0)
                return false;
            if (((Facultad)elementoActual()).Nombre == filtros[0].ToString())
                return true;
            return false;
        }
    }
}