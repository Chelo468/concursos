﻿using EntregaConcursos.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntregaConcursos.Models.Soporte
{
    public class IteradorCarrera : IIterador
    {
        private Catedra[] catedras;
        private int actual;

        public void crear(object[] catedras)
        {
            this.catedras = catedras as Catedra[];
            actual = -1;
        }

        public object elementoActual()
        {
            // Si no existen elementos, devolveremos null.
            // Si el indice actual es mayor que el mayor indice aceptable, devolveremos null.
            // Si el indice actual es -1, devolveremos null.
            if ((this.catedras == null) || (this.catedras.Length == 0) ||
                (actual > this.catedras.Length - 1) || (actual < 0))
                return null;

            // Devolvemos el elemento correspondiente al elemento actual
            else
                return catedras[actual];
        }

        public bool haTerminado()
        {
            return actual >= catedras.Length;
        }

        public object primero()
        {
            try
            {
                actual = 0;
                return catedras[0];
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void setLista(object[] catedras)
        {
            this.catedras = catedras as Catedra[];
        }

        public void setPosicion(int posicion)
        {
            actual = posicion;
        }

        public object siguiente()
        {
            // Si no existen elementos, devolveremos null.
            // Si el indice siguiente es mayor que el mayor indice aceptable, devolveremos null.
            if ((catedras == null) ||
                (catedras.Length == 0) ||
                (actual + 1 > catedras.Length - 1))
            {
                actual++;
                return null;
            }

            // Aumentamos el índice en una unidad y devolvemos ese elemento
            else
            {
                actual++;
                return catedras[actual];
            }
        }

        public bool cumpleFiltros(string[] filtros)
        {
            if (filtros.Length > 1 || filtros.Length < 0)
                return false;
            if (((Catedra)elementoActual()).Nombre == filtros[0].ToString())
                return true;
            return false;
        }
    }
}