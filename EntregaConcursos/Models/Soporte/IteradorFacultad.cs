﻿using EntregaConcursos.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntregaConcursos.Models.Soporte
{
    public class IteradorFacultad : IIterador
    {
        private Carrera[] carreras;
        private int actual;

        public void crear(object[] carreras)
        {
            this.carreras = carreras as Carrera[];
            actual = -1;
        }

        public object elementoActual()
        {
            // Si no existen elementos, devolveremos null.
            // Si el indice actual es mayor que el mayor indice aceptable, devolveremos null.
            // Si el indice actual es -1, devolveremos null.
            if ((this.carreras == null) || (this.carreras.Length == 0) ||
                (actual > this.carreras.Length - 1) || (actual < 0))
                return null;

            // Devolvemos el elemento correspondiente al elemento actual
            else
                return carreras[actual];
        }

        public bool haTerminado()
        {
            return actual >= carreras.Length;
        }

        public object primero()
        {
            try
            {
                actual = 0;
                return carreras[0];
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void setLista(object[] facultades)
        {
            this.carreras = facultades as Carrera[];
        }

        public void setPosicion(int posicion)
        {
            actual = posicion;
        }

        public object siguiente()
        {
            // Si no existen elementos, devolveremos null.
            // Si el indice siguiente es mayor que el mayor indice aceptable, devolveremos null.
            if ((carreras == null) ||
                (carreras.Length == 0) ||
                (actual + 1 > carreras.Length - 1))
            {
                actual++;
                return null;
            }

            // Aumentamos el índice en una unidad y devolvemos ese elemento
            else
            {
                actual++;
                return carreras[actual];
            }
        }

        public bool cumpleFiltros(string[] filtros)
        {
            if (filtros.Length > 1 || filtros.Length < 0)
                return false;
            if (((Carrera)elementoActual()).Nombre == filtros[0].ToString())
                return true;
            return false;
        }
    }
}