﻿using EntregaConcursos.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntregaConcursos.Models.Soporte.Estados
{
    public class Anulado : EstadoConcurso
    {
        public override bool esAnulado()
        {
            return true;
        }
    }
}