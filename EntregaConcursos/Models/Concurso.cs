﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntregaConcursos.Models
{
    public class Concurso
    {
        #region atributos

        private DateTime fechaCreacion { get; set; }
        private int numeroExpediente { get; set; }
        private Catedra catedra { get; set; }
        private DateTime fechaEstimadaConvocatoriaInscripcion { get; set; }
        private DateTime fechaEstimadaRealizacion { get; set; }
        private List<HistorialEstadoConcurso> estados { get; set; }
        private DateTime fechaDesdeInscripcionAspirantes { get; set; }
        private DateTime fechaFinInscripcionAspirantes { get; set; }
        private DateTime fechaLimiteRecepcionTemas { get; set; }
        private DateTime fechaSorteoTemas { get; set; }
        private DateTime fechaClasePublica { get; set; }
        private DateTime fechaDesdeExcusacion { get; set; }
        private DateTime fechaLimiteExcusacion { get; set; }
        private DateTime fechaDesdeRecusacion { get; set; }
        private DateTime fechaLimiteRecusacion { get; set; }
        private List<CargoLlamado> cargos { get; set; }
        private List<MiembroTribunal> tribunal { get; set; }
        private DateTime fechaHoraNotificacionRealizacion { get; set; }
        private Sorteo sorteo { get; set; }
        private string lugarDesignado { get; set; }
        private EjecucionConcurso sustanciacion { get; set; }
        #endregion

        #region propiedades
        public DateTime FechaCreacion
        {
            get { return fechaCreacion; }
            set { fechaCreacion = value; }
        }

        public int NumeroExpediente
        {
            get { return numeroExpediente; }
            set { numeroExpediente = value; }
        }

        public DateTime FechaEstimadaConvocatoriaInscripcion
        {
            get { return fechaEstimadaConvocatoriaInscripcion; }
            set { fechaEstimadaConvocatoriaInscripcion = value; }
        }

        public DateTime FechaEstimadaRealizacion
        {
            get { return fechaEstimadaRealizacion; }
            set { fechaEstimadaRealizacion = value; }
        }

        public List<HistorialEstadoConcurso> Estados
        {
            get { return estados; }
            set { estados = value; }
        }

        public DateTime FechaDesdeInscripcionAspirantes
        {
            get { return fechaDesdeInscripcionAspirantes; }
            set { fechaDesdeInscripcionAspirantes = value; }
        }

        public DateTime FechaLimiteRecepcionTemas
        {
            get { return fechaLimiteRecepcionTemas; }
            set { fechaLimiteRecepcionTemas = value; }
        }
        public DateTime FechaSorteoTemas
        {
            get { return fechaSorteoTemas; }
            set { fechaSorteoTemas = value; }
        }
        public DateTime FechaClasePublica
        {
            get { return fechaClasePublica; }
            set { fechaClasePublica = value; }
        }

        public DateTime FechaDesdeExcusacion
        {
            get { return fechaDesdeExcusacion; }
            set { fechaDesdeExcusacion = value; }
        }

        public DateTime FechaLimiteExcusacion
        {
            get { return fechaLimiteExcusacion; }
            set { fechaLimiteExcusacion = value; }
        }

        public DateTime FechaDesdeRecusacion
        {
            get { return fechaDesdeRecusacion; }
            set { fechaDesdeRecusacion = value; }
        }

        public DateTime FechaLimiteRecusacion
        {
            get { return fechaLimiteRecusacion; }
            set { fechaLimiteRecusacion = value; }
        }

        public List<CargoLlamado> Cargos
        { get { return cargos; }
          set { cargos = value; }
        }

        public List<MiembroTribunal> Tribunal
        {
            get { return tribunal; }
            set { tribunal = value; }
        }

        public DateTime FechaHoraNotificacionRealizacion
        {
            get { return fechaHoraNotificacionRealizacion; }
            set { fechaHoraNotificacionRealizacion = value; }
        }

        public Sorteo Sorteo
        {
            get { return sorteo; }
            set { sorteo = value; }
        }

        public string LugarDesignado
        {
            get { return lugarDesignado; }
            set { lugarDesignado = value; }
        }

        public EjecucionConcurso Sustanciacion
        {
            get { return sustanciacion; }
            set { sustanciacion = value; }
        }
        #endregion

        #region operaciones
        public Concurso()
        {
            fechaCreacion = DateTime.Now;
        }
        #endregion
    }
}