﻿using EntregaConcursos.Models.Interfaces;
using EntregaConcursos.Models.Soporte;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntregaConcursos.Models
{
    public class Universidad : IAgregado
    {
        //private int idUniversidad { get; set; }
        private string nombre { get; set; }
        private string sigla { get; set; }
        private List<Facultad> facultades { get; set; }
        private IIterador iteradorFacultades { get; set; }
        //private IIterador iteradorUniversidad { get; set; }
        //private Agregado facultad { get; set; }

        public Universidad()
        {
            facultades = new List<Facultad>();
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Sigla
        {
            get { return sigla; }
            set { sigla = value; }
        }

        public List<Facultad> Facultades
        {
            get { return facultades; }
            set { facultades = value; }
        }

        public IIterador crearIterador(object[] facultades)
        {
            return new IteradorUniversidad(facultades);
        }
    }
}