﻿using System;
using System.Collections.Generic;

namespace EntregaConcursos.Models
{
    public class Inscripcion
    {
        

        public DateTime fechaInscripcion { get; set; }
        public Docente docente { get; set; }
        public List<TurnoPresentacionAntecedentes> turnos  { get; set; }
        public List<AntecedentesCVAR> antecedentes { get; set; }
        public Boolean antecedentesChequeados { get; set; }
        public SolicitudRecusacion solicitudRecusacion { get; set; }
        public EstadoInscripcion estado { get; set; }

        public Inscripcion(DateTime fechaInscripcion, Docente docente, List<TurnoPresentacionAntecedentes> turnos, List<AntecedentesCVAR> antecedentes, bool antecedentesChequeados, SolicitudRecusacion solicitudRecusacion, EstadoInscripcion estado)
        {
            this.fechaInscripcion = fechaInscripcion;
            this.docente = docente;
            this.turnos = turnos;
            this.antecedentes = antecedentes;
            this.antecedentesChequeados = antecedentesChequeados;
            this.solicitudRecusacion = solicitudRecusacion;
            this.estado = estado;
        }

    }
}