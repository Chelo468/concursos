﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntregaConcursos.Models
{
    public class CargoLlamado
    {
     

        private Categoria categoria { get; set; }
        private int cantidad { get; set; }
        public List<Inscripcion> inscripciones { get; set; }

        public CargoLlamado(Categoria categoria, int cantidad, List<Inscripcion> inscripciones)
        {
            this.categoria = categoria;
            this.cantidad = cantidad;
            this.inscripciones = inscripciones;
        }

        public int Cantidad
        {
            get { return cantidad; }
            set { cantidad = value; }
        }

        public List<Inscripcion> Catedras
        {
            get { return inscripciones; }
            set { inscripciones = value; }
        }

        public Categoria Categoria
        {
            get { return categoria; }
            set { categoria = value; }
        }


    }

   
}