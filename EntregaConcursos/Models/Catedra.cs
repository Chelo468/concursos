﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntregaConcursos.Models
{
    public class Catedra
    {
        public Catedra(string codigo, string nombre, int nivel, string sigla)
        {
            this.codigo = codigo;
            this.nombre = nombre;
            this.nivel = nivel;
            this.sigla = sigla;
        }

        private string codigo { get; set; }
        private string nombre { get; set; }
        private int nivel { get; set; }
        private string sigla { get; set; }

        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public int Nivel
        {
            get { return nivel; }
            set { nivel = value; }
        }

        public string Sigla
        {
            get { return sigla; }
            set { sigla = value; }
        }


    }
}