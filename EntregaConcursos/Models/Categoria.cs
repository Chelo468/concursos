﻿namespace EntregaConcursos.Models
{
    public class Categoria
    {
        private string nombre { get; set; }
        private string descripcion { get; set; }

        public Categoria(string nombre, string descripcion)
        {
            this.nombre = nombre;
            this.descripcion = descripcion;
        }

        
 
       
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
    }
}