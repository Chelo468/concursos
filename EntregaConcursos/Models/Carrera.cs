﻿using EntregaConcursos.Models.Interfaces;
using EntregaConcursos.Models.Soporte;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntregaConcursos.Models
{
    public class Carrera
    {
        private string nombre { get; set; }
        private string descripcion { get; set; }
        private List<Catedra> catedras { get; set; }

        public Carrera(string nombre, string descripcion, List<Catedra> catedras)
        {
            this.nombre = nombre;
            this.descripcion = descripcion;
            this.catedras = catedras;
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        public List<Catedra> Catedras
        {
            get { return catedras; }
            set { catedras = value; }
        }

        public IIterador crearIterador(object[] elementos)
        {
            IteradorCarrera iterador = new IteradorCarrera();
            iterador.crear(catedras.ToArray());
            return iterador;
        }

    }
}