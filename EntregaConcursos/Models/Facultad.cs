﻿using EntregaConcursos.Models.Interfaces;
using EntregaConcursos.Models.Soporte;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntregaConcursos.Models
{
    public class Facultad : IAgregado
    {
        private string nombre { get; set; }
        private List<Carrera> carreras { get; set; }

        public Facultad(string nombre)
        {
            this.nombre = nombre;
            carreras = new List<Carrera>();
        }

        public Facultad(string nombre, List<Carrera> carreras)
        {
            this.nombre = nombre;
            this.carreras = carreras;
        }

        public IIterador crearIterador(object[] elementos)
        {
            IteradorFacultad iterador = new IteradorFacultad();
            iterador.setLista(carreras.ToArray());
            return iterador;
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public List<Carrera> Carreras
        {
            get { return carreras; }
            set { carreras = value; }
        }
    }
}